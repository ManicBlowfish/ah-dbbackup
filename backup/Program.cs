﻿
using System;
using System.Configuration;
using System.Net.Mail;
using Quartz;
using Quartz.Impl;
namespace backup
{
    class Program
    {
        static void Main(string[] args)
        {
            // construct a scheduler
            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<BackupJob>().Build();

            var trigger = TriggerBuilder.Create()
                            .WithSimpleSchedule(x => x.WithIntervalInHours(24).RepeatForever())
                            .StartAt(DateTime.Now.Date.AddHours(6))  // 6am UTC, early morning EST
                            .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }

    class BackupJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                new DbScripter().ScriptDb();
                SendMail("Backup Succeeded", "done!");
            }
            catch (Exception ex)
            {
                SendMail("Backup Failed", ex.ToString());
            }
        }

        private void SendMail(string status, string detail)
        {
            SmtpClient smtp = new SmtpClient();
            MailMessage msg = new MailMessage();
            msg.To.Add(ConfigurationManager.AppSettings["ToEmail"]);
            msg.Subject = status;
            msg.Body = detail;
            smtp.Send(msg);
        }
    }
}
